# Contributing to Icy

First off, thanks for taking the time to contribute to Icy ! :+1:

The following is a set of article references allowing you to contribute to Icy in general.

## Code of Conduct

The project and everyone participating in it is governed by the [Icy Code of Conduct](CODE-OF-CONDUCT.md). By participating, you are expected to uphold this code.

## I have a question or just want to get help !!!

> **Note:** Please don't fill an issue to ask a question !

You have several ways to get helps for Icy, be sure to read the [Assistance Guide](http://icy.bioimageanalysis.org/tutorial/how-to-get-help-on-icy) to use the best one for your needs.

## How Can I Contribute?

### Reporting Bugs

For bug report, consult our [Assistance Guide](http://icy.bioimageanalysis.org/tutorial/how-to-get-help-on-icy) which provide good practises about how submitting a bug report. Following these guidelines helps maintainers and the community to understand your report, reproduce the behavior and so eventually fixing the problem.

### Publishing a script or a protocol

If you developed a script or a protocol in Icy and you think it can be useful to others people then why not sharing it ??

You can follow instructions from the [How to publish a Protocol](http://icy.bioimageanalysis.org/tutorial/how-to-publish-a-protocol/)  article. It gives you the different steps to publish a protocol on the Icy Website making it available to everyone directly through the appliation. The article describe the procedure for protocol but you can perfectly apply it to a script as well.

### Creating a new plugin

If you are a developer and want to create your own plugin for Icy then you should give have a look to the [Create a new plugin for Icy](http://icy.bioimageanalysis.org/developer/create-a-new-icy-plugin/) guide.

### Contributing directly to Icy or to an existing plugin (bug fixes, enhancements..)

If you are a developer and want to contribute to Icy development or to a plugin hosted in the [BIA group Gitlab](https://gitlab.pasteur.fr/bia) then you should follow instructions from our [Contributing to Icy development](http://icy.bioimageanalysis.org/developer/contributing-icy-development) guide.
