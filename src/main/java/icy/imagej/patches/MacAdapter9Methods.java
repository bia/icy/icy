/*
 * Copyright 2010-2015 Institut Pasteur.
 * 
 * This file is part of Icy.
 * 
 * Icy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Icy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Icy. If not, see <http://www.gnu.org/licenses/>.
 */
package icy.imagej.patches;

import ij.plugin.MacAdapter9;

/**
 * @author Stephane
 */
public class MacAdapter9Methods
{
    private MacAdapter9Methods()
    {
        // prevent instantiation of utility class
    }

    public static void run(final MacAdapter9 obj)
    {
        // do nothing
    }

    public static void run(final MacAdapter9 obj, String event)
    {
        // do nothing
    }

    public static void handleAbout(final MacAdapter9 obj, Object event)
    {
        // do nothing
    }

    public static void openFiles(final MacAdapter9 obj, Object event)
    {
        // do nothing
    }

    public static void handleQuitRequestWith(final MacAdapter9 obj, Object event, Object response)
    {
        // do nothing
    }
}
