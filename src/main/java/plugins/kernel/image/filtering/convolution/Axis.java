package plugins.kernel.image.filtering.convolution;

public enum Axis
{
    X, Y, Z
}
