package plugins.kernel.image.filtering.convolution;

public interface IKernel1D extends IKernel
{
    boolean isSeparable();
}
